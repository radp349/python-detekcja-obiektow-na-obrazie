import cv2
import numpy as np
import matplotlib.pyplot as plt


fig, (ax1) = plt.subplots(1)

org_img = cv2.imread("0700/org.jpg")
edited_img = cv2.imread("0700/edited.jpg")

org_gray = cv2.cvtColor(org_img, cv2.COLOR_BGR2GRAY)
edited_gray = cv2.cvtColor(edited_img, cv2.COLOR_BGR2GRAY)

kernel = np.ones((3, 3), np.uint8)

result = cv2.subtract(edited_gray, org_gray)
result1 = cv2.subtract(org_gray, edited_gray)
result = cv2.add(result, result1)
result = cv2.Canny(result, 200 ,201)

img_dilation = cv2.dilate(result, kernel, iterations=5)
img_erosion = cv2.erode(img_dilation, kernel, iterations=7)

# sure background area
sure_bg = img_dilation
# Finding unknown region
sure_fg = img_erosion
unknown = cv2.subtract(sure_bg, sure_fg)




# Marker labelling
ret, markers = cv2.connectedComponents(sure_fg)

# Add one to all labels so that sure background is not 0, but 1
markers = markers +1
# Now, mark the region of unknown with zero
markers[unknown == 255] = 0


copy = edited_img.copy()
markers = cv2.watershed(copy, markers)

copy[markers == 1] = [255, 255, 255]


contours = cv2.findContours(img_dilation.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
if len(contours) == 2:
    contours = contours[0] 
else:
    contours = contours[1]

mask = np.zeros(edited_img.shape, dtype='uint8')
Countour_number = 1

for c in contours:
    area = cv2.contourArea(c)
    if area > 40:
        x, y, w, h = cv2.boundingRect(c)
        ROI = copy[y:y+h, x:x+w]
        cv2.imwrite('Grinch_{}.png'.format(Countour_number), ROI)
        cv2.rectangle(copy, (x, y), (x + w, y + h), (36, 255, 12), 2)
        cv2.rectangle(edited_img, (x, y), (x + w, y + h), (36, 255, 12), 2)
        Countour_number += 1
x = cv2.add(org_img,copy)
edited_img = cv2.cvtColor(edited_img, cv2.COLOR_RGB2BGR)
ax1.imshow(edited_img)
edited_img = cv2.cvtColor(edited_img, cv2.COLOR_BGR2RGB)
cv2.imwrite('GrinchsBackground.png', edited_img)
cv2.imwrite('Grinchs.png', copy)


plt.show()

