# Readme

## Wprowadzenie

Ten kod służy do analizy i wyodrębniania różnic pomiędzy dwoma obrazami. Po wykryciu różnic, kod generuje maski tych obszarów, tworzy kontury i zapisuje te fragmenty jako oddzielne obrazy. 

## Wymagania

Aby uruchomić ten kod, musisz mieć zainstalowane następujące biblioteki:

- OpenCV (cv2): Jest to biblioteka używana do przetwarzania obrazów i filmów.
- Numpy: Jest to biblioteka używana do obliczeń naukowych.
- Matplotlib: Jest to biblioteka używana do generowania wykresów.

## Jak działa ten kod?

1. Najpierw kod wczytuje dwa obrazy: oryginalny ("org.jpg") i edytowany ("edited.jpg"). 

2. Następnie obrazy są konwertowane na odcienie szarości.

3. Kod następnie odejmuje od siebie obrazy, aby uzyskać różnice pomiędzy nimi.

4. Aby wyodrębnić obszary różnic, stosuje operację Canny Edge Detection, a następnie dilatację i erozję.

5. Znajduje obszary pewne jako tło i nieznane. 

6. Następnie wyznacza markery dla różnych regionów obrazu, a następnie stosuje algorytm Watershed, aby oddzielić różne obszary obrazu.

7. Następnie kod wyszukuje kontury w obrazie i dla każdego konturu, który ma powierzchnię większą niż 40 pikseli, tworzy Region Of Interest (ROI), zapisuje go jako oddzielny obraz i rysuje prostokąt wokół ROI na obrazie.

8. Na koniec kod zapisuje wyjściowy obraz z oznaczonymi różnicami.

## Użycie

Aby użyć tego kodu, należy umieścić obrazy do porównania w odpowiednim katalogu i uruchomić skrypt. Obrazy wynikowe zostaną zapisane w katalogu roboczym z odpowiednimi nazwami.

Uwaga: Ten kod został napisany z założeniem, że obrazy wejściowe są w katalogu "0700". Upewnij się, że ścieżka do obrazów jest poprawna, zanim uruchomisz skrypt.

## Wyjście

Skrypt zapisze obrazy wynikowe, które pokazują obszary różniące się między dwoma obrazami. Każdy odrębny obszar różnicy zostanie zapisany jako oddzielny obraz.
